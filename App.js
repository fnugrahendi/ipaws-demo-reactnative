import React from 'react'
import { SafeAreaView } from 'react-native'
import { Provider } from 'react-redux'
import DrawerNavigator from './src/navigation/drawerNavigator'
import store from './src/state/store'

import SplashScreen from 'react-native-splash-screen'

export default class App extends React.Component {
  componentDidMount() {
    SplashScreen.hide()
  }
  render() {
    return (
      <Provider store={store}>
        <SafeAreaView style={{ flex: 1 }}>
          <DrawerNavigator />
        </SafeAreaView>
      </Provider>
    );
  }
}