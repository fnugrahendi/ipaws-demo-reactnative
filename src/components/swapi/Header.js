import React from 'react';
import { View, Button, StyleSheet, Text } from 'react-native';
import { Mode } from '../../constants/enum';

export default class Header extends React.Component {
    render(){
        return (
            <View style={{flex: 1, flexDirection: 'column'}}>
                <View style={{flexDirection: 'row', height: 50, justifyContent: 'center'}}>
                        {
                            Object.keys(Mode).map((key, id) => {
                                return <Button key={`${key}${id}`} onPress={() => this.props.onChangeMode(Mode[key])} title={Mode[key]}/>
                            })
                        }
                </View>
            </View>
        )
    }
}