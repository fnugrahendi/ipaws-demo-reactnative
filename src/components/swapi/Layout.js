import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import ListContainer from './ListContainer';
import Header from './Header';
import { Mode } from '../../constants/enum';

export default class Layout extends React.Component {
    state = {
        mode: Mode.PEOPLE
    }
    onChangeMode = (val) => {
        if (this.state.mode !== val)
            this.setState({ mode: val });
    }
    render() {
        return (
            <View style={styles.container}>
                <Header onChangeMode={this.onChangeMode} />
                <ListContainer mode={this.state.mode} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: "center",
        // backgroundColor: '#CCCCCC',
        justifyContent: 'center',
    },
});