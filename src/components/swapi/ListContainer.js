import React from 'react'
import { ScrollView, View, Text, ActivityIndicator } from 'react-native'
// import * as swapi from '../../api/swapi'
import { Mode } from '../../constants/enum'
import { connect } from 'react-redux'
import { swapiActions } from '../../state/ducks/swapi/'

class ListContainer extends React.Component {
    componentDidMount() {
        this.props.fetchPeople()
    }

    componentDidUpdate(prevProps) {
        const {
            mode, fetchFilms, fetchPeople, fetchVehicles,
            people, films, vehicles
        } = this.props
        if (mode !== prevProps.mode) {
            switch (mode) {
                case Mode.FILMS:
                    if (!films || films.length == 0)
                        fetchFilms()
                    break;
                case Mode.VEHICLES:
                    if (!vehicles || vehicles.length == 0)
                        fetchVehicles()
                    break;
                case Mode.PEOPLE:
                default:
                    if (!people || people.length == 0)
                        fetchPeople()
                    break;
            }
        }
    }

    render() {
        const { mode, people, vehicles, films, isFetching } = this.props
        const list = mode == Mode.FILMS ? films : mode == Mode.VEHICLES ? vehicles : people
        return (
            <ScrollView style={{ flex: 1, marginLeft: 20 }}>
                 {
                    (list && list.length > 0) ?
                    list.map((data, id) => {
                        return <Text style={{ margin: 3 }} key={id}>{data.name ? data.name : data.title}</Text>
                    }) :
                    <React.Fragment>
                        <ActivityIndicator
                            size="large"
                            color="#0000FF"
                        />
                        <Text>Loading the force ...</Text>
                    </React.Fragment>
                }
            </ScrollView>
        );
    }
}

const mapStateToProps = (state) => {
    let { people, isFetching, errorFetching, films, vehicles } = state.swapiState
    return {
        people, isFetching, errorFetching, films, vehicles
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchPeople: () => dispatch(swapiActions.fetchPeople()),
        fetchFilms: () => dispatch(swapiActions.fetchFilms()),
        fetchVehicles: () => dispatch(swapiActions.fetchVehicles()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer)