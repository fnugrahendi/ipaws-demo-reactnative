import axios from 'axios';

const baseUrl = 'https://swapi.co/api';

export const getAllPeople = () => {
    const peopleApiURL = `${baseUrl}/people/`;
    return axios.get(peopleApiURL);
}

export const getAllFilms = () => {
    const filmApiURL = `${baseUrl}/films/`;
    return axios.get(filmApiURL);
}

export const getAllVehicles = () => {
    const vehiclesApiURL = `${baseUrl}/vehicles/`;
    return axios.get(vehiclesApiURL);
}

export const getDataByUrl = (url) => {
    return axios.get(url);
}
