import React, {Component} from 'react'
import Layout from '../components/swapi/Layout'

export default class SwapiScreen extends Component {
    static navigationOptions = {
        title: 'SWAPI'
    }
    componentDidMount = () => {
        this._initScreen()
    }

    _initScreen = async () => {
        this.props.navigation.setParams({pageTitle: 'SWAPI'})
    }

    render() {
        return (
            <Layout/>
        )
    }
}