import HomeScreen from './HomeScreen'
import AboutUsScreen from './AboutUsScreen'
import SwapiScreen from './SwapiScreen'

export {
    HomeScreen,
    AboutUsScreen,
    SwapiScreen,
}