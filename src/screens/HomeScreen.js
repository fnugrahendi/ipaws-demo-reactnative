import React from 'react'
import { View, Text, Button } from 'react-native'

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Home'
  }
  componentDidMount = () => {
    this._initScreen()
  }
  _initScreen = async () => {
    this.props.navigation.setParams({ pageTitle: 'HOME' })
  }
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Text>Home Screen</Text>
        <Button
          title="OPEN DRAWER"
          onPress={() => this.props.navigation.openDrawer()}
        />
      </View>
    );
  }
}