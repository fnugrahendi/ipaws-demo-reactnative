import React, { Component } from 'react'
import {
    View,
    Dimensions,
    Text
} from 'react-native'

export default class AboutUsScreen extends Component {
    static navigationOptions = {
        title: 'About'
    }
    componentDidMount = () => {
        this._initScreen()
    }
    _initScreen = async () => {
        this.props.navigation.setParams({ pageTitle: 'ABOUT US' })
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>
                    About Us Screen
                </Text>
            </View>
        )
    }
}

const styles = {
    container: {
        alignItems: 'center',
        flexDirection: 'column',
        backgroundColor: "#FFFFFF",
        width: Dimensions.get('window').width,
        // minHeight: '100%'
    }
}