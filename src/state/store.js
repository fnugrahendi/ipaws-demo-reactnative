import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import thunk from 'redux-thunk'
import * as reducers from './ducks'

// export default function configureStore(initialState = {}) {
//     const rootReducer = combineReducers(reducers)
//     const middlewares = [thunk]
//     return createStore(
//         rootReducer,
//         initialState,
//         compose(applyMiddleware(...middlewares))
//     )
// }

const middlewares = [thunk]
const store = createStore(
    combineReducers(reducers),
    compose(applyMiddleware(...middlewares))
)

export default store