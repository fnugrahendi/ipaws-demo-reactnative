import * as types from './types'

const initialState = {
    isFetching: false,
    errorFetching: false,
    people: [],
    films: [],
    vehicles: [],
}

export const swapiReducers = (state = initialState, action) => {
    switch (action.type) {
        case types.FETCH_REQUEST:
            return {
                ...state,
                isFetchingPeople: true,
                errorFetchingPeople: false,
            }
        case types.FETCH_PEOPLE_SUCCESS:
            return {
                ...state,
                people: action.payload.results,
                isFetchingPeople: false,
                errorFetchingPeople: false,
            }
        case types.FETCH_VEHICLES_SUCCESS:
            return {
                ...state,
                vehicles: action.payload.results,
                isFetchingPeople: false,
                errorFetchingPeople: false,
            }
        case types.FETCH_FILMS_SUCCESS:
            return {
                ...state,
                films: action.payload.results,
                isFetchingPeople: false,
                errorFetchingPeople: false,
            }
        case types.FETCH_FAILURE:
            return {
                ...state,
                isFetchingPeople: false,
                errorFetchingPeople: true,
            }
        default: return state
    }
}