import { swapiReducers } from './reducers'

// export { default as swapiOperations } from './operations'
export { default as swapiActions } from './actions'
export { default as swapiTypes } from './types'

export default swapiReducers