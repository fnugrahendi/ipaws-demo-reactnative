import * as types from "./types"
import { getRequest } from '../../../utilities/request'

const fetchRequest = () => {
    return ({
        type: types.FETCH_REQUEST
    })
}

const fetchFailure = (error) => {
    return ({
        type: types.FETCH_FAILURE,
        payload: error
    })
}

const fetchPeopleSuccess = (data) => {
    return ({
        type: types.FETCH_PEOPLE_SUCCESS,
        payload: data
    })
}

const fetchVehiclesSuccess = (data) => {
    return ({
        type: types.FETCH_VEHICLES_SUCCESS,
        payload: data
    })
}

const fetchFilmsSuccess = (data) => {
    return ({
        type: types.FETCH_FILMS_SUCCESS,
        payload: data
    })
}

export const fetchPeople = () => dispatch => {
    dispatch(fetchRequest())
    getRequest({
        path: '/people/',
        type: 'swapi',
        successCallback: (response) => {
            dispatch(fetchPeopleSuccess(response.data))
        },
        failureCallback: (error) => {
            dispatch(fetchFailure(error))
        }
    })
}

export const fetchVehicles = () => dispatch => {
    dispatch(fetchRequest())
    getRequest({
        path: '/vehicles/',
        type: 'swapi',
        successCallback: (response) => {
            dispatch(fetchVehiclesSuccess(response.data))
        },
        failureCallback: (error) => {
            dispatch(fetchFailure(error))
        }
    })
}

export const fetchFilms = () => dispatch => {
    dispatch(fetchRequest())
    getRequest({
        path: '/films/',
        type: 'swapi',
        successCallback: (response) => {
            dispatch(fetchFilmsSuccess(response.data))
        },
        failureCallback: (error) => {
            dispatch(fetchFailure(error))
        }
    })
}



export default {
    fetchPeople,
    fetchVehicles,
    fetchFilms
}