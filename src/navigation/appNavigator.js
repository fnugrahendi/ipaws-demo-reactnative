import { createAppContainer, createStackNavigator } from 'react-navigation'
import DrawerNavigator from './drawerNavigator'
import BottomNavigator from './bottomNavigator'

const AppNavigator = createStackNavigator({
    Home: {
        screen: DrawerNavigator,
    },
    Bottom: {
        screen: BottomNavigator,
    },
})

export default createAppContainer(AppNavigator)