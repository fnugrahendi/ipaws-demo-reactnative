import {
    HomeScreen,
    AboutUsScreen,
    SwapiScreen,
} from '../screens'
import BottomNavigator from './bottomNavigator'

import { createDrawerNavigator, createStackNavigator, createAppContainer } from 'react-navigation'
const MyDrawerNavigator = createDrawerNavigator({
    Home: BottomNavigator,
    SWAPI: SwapiScreen,
    About: AboutUsScreen,
},
{
    initialRouteName: 'Home'
});

const DrawerNavigator = createAppContainer(MyDrawerNavigator);

export default DrawerNavigator