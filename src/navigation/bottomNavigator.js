import React from "react";
import {
  AboutUsScreen,
  SwapiScreen,
  HomeScreen,
} from '../screens'
import { createBottomTabNavigator, createAppContainer } from "react-navigation";

const BottomNavigator = createBottomTabNavigator({
  Home: HomeScreen,
  About: AboutUsScreen,
  SWAPI: SwapiScreen,
}, {
  initialRouteName: 'Home'
});

export default createAppContainer(BottomNavigator);