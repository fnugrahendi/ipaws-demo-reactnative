// check if given params is a JSON string
export const isJSONString = (json) => {
    try {
        JSON.parse(json)
    } catch(error) {
        return false
    }
    return true
}