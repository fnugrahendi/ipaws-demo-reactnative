import axios from 'axios'
import { baseSwapiUrl } from '../config/settings'
import {isJSONString} from './jsonHelper'

const axiosInstance = (type) => {
    // currently only support swapi url
    let baseAPIURL = type == 'swapi' ? baseSwapiUrl : baseSwapiUrl
    return axios.create({
        baseURL: baseAPIURL,
        timeout: 10000,
    })
}

export const getRequest = async ({path, type, successCallback, failureCallback}) => {
    let apiCaller = await axiosInstance(type)
    try {
        let response = await apiCaller.get(path)
        response = isJSONString(response) ? JSON.parse(response) : response
        successCallback(response)
    } catch (error) {
        failureCallback(error)
    }
}

